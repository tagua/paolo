/*
  Copyright (c) 2007 Paolo Capriotti <p.capriotti@gmail.com>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
*/

#include "test_utils.h"
#include <iostream>
#include <QString>
#include <color.h>
#include <piece.h>
#include <type.h>

std::ostream& operator<<(std::ostream& os, const Piece& piece) {
  if (piece.color() && piece.type())
    return os << qPrintable(piece.color()->name()) 
        << " " << qPrintable(piece.type()->name());
  else
    return os << "<no piece>";
}

std::ostream& operator<<(std::ostream& os, const QString& str) {
  return os << qPrintable(str);
}
