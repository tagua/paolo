/*
  Copyright (c) 2007 Paolo Capriotti <p.capriotti@gmail.com>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
*/

#ifndef TEST_UTILS_H
#define TEST_UTILS_H

#include <iosfwd>

class Piece;
class QString;

std::ostream& operator<<(std::ostream& os, const Piece& piece);
std::ostream& operator<<(std::ostream& os, const QString& str);

#endif // TEST_UTILS_H
