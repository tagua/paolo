/*
  Copyright (c) 2006 Paolo Capriotti <p.capriotti@gmail.com>
            (c) 2006 Maurizio Monge <maurizio.monge@kdemail.net>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
*/

#ifndef GRAPHICALSYSTEM_H
#define GRAPHICALSYSTEM_H

#include <boost/shared_ptr.hpp>
#include <QObject>
#include <core/taguaapi.h>
#include <core/piece.h>
#include <core/state_fwd.h>
#include <core/namedsprite.h>

class ChessBoard;
class ChessTable;
class Components;
class IAnimator;
class INamer;
class IState;
class Move;
class IndexConverter;
class PointConverter;
class UserEntity;

class GraphicalSystem : public QObject,
                        private TaguaAPI {
Q_OBJECT

public:

  /** The current chess table */
  ChessTable* m_view;

  /** The current board */
  // FIXME: why is this needed? couldn't it be extracted from the view dynamically?
  ChessBoard* m_board;

  /** The current state */
  StatePtr m_state;

  /** The variant specific animator */
  IAnimator* m_animator;

  Components* m_components;

  /** @a GraphicalPosition interface function implementation */
//   virtual void addTag(const QString& name, Point, bool over = false);

  /** @a GraphicalPosition interface function implementation */
//   virtual void clearTags(const QString& name);

  /** @a GraphicalPosition interface function implementation */
//   virtual void setTags(const QString& name, Point p1 = Point::invalid(), Point p2 = Point::invalid(),
//                                       Point p3 = Point::invalid(), Point p4 = Point::invalid(),
//                                       Point p5 = Point::invalid(), Point p6 = Point::invalid() );

public Q_SLOTS:
  /** internal function to listen at setting changes */
  void settingsChanged();

private:
  /**
    * @return the board point converter. (interface for TaguaAPI)
    */
  virtual const PointConverter* converter() const;
  
  /**
    * @return the index converter of the specified pool.
    */
  virtual const IndexConverter* indexConverter(int pool) const;

  /**
    * @return the current abstract position. (interface for TaguaAPI)
    */
  virtual const IState* state() const;

  /**
    * @return a sprite at the position @a index in the graphical pool. (interface for TaguaAPI)
    */
  virtual NamedSprite getSprite(const Point& p);

  /**
    * Removes a sprite at the position @a index in the graphical pool. (interface for TaguaAPI)
    * @return the newly created sprite.
    */
  virtual NamedSprite takeSprite(const Point& p);

  /**
    * Sets the piece at the position @a index in the graphical pool. (interface for TaguaAPI)
    * @return the newly created sprite.
    */
  virtual NamedSprite setPiece(const Point& p, const Piece& piece, bool show);

  /**
    *  Create a new piece, but do not add it to the graphical system.
    * @return the newly created sprite.
    */
  virtual NamedSprite createPiece(const Point& p, const Piece& piece, bool show);

  /**
    * Sets the sprite at the position @a index in the graphical pool. (interface for TaguaAPI)
    * @return the newly created sprite.
    */
  virtual void setSprite(const Point& p, const NamedSprite& sprite);

  /**
    * @return how many sprites are contained in the pool
    */
  virtual int poolSize(int pool);

  /**
    * @return the sprite at the position @a index in the graphical pool. (interface for TaguaAPI)
    */
  virtual NamedSprite getPoolSprite(const IColor* pool, int index);

  /**
    * Removes the sprite at the position @a index in the graphical pool. (interface for TaguaAPI)
    */
  virtual void removePoolSprite(const IColor* pool, int index);

  /**
    * Removes the sprite at the position @a index in the graphical pool (only for drops).
    * (interface for TaguaAPI)
    * @return the removed sprite.
    */
  virtual NamedSprite takePoolSprite(const IColor* pool, int index);

  /**
    * Inserts a sprite at the position @a index in the graphical pool. (interface for TaguaAPI)
    * @return the newly created sprite.
    */
  virtual NamedSprite insertPoolPiece(const IColor* pool, 
                                      int index, 
                                      const Piece& piece);

  /**
    * @return the piece of the pool that has been dropped, or (-1,-1). (interface for TaguaAPI)
    */
  virtual std::pair<const IColor*, int> droppedPoolPiece();

public:
  /** Constructor */
  GraphicalSystem(ChessTable* view, const StatePtr& startingPosition, Components* components);
  virtual ~GraphicalSystem();

  /** Sets the reference entity */
  void setup(const boost::shared_ptr<UserEntity>& entity);

  /** Goes forward playing a move, that has to be legal and checked */
  void forward(const Move& move,
               const StatePtr& pos,
               const SpritePtr& = SpritePtr());

  /** Goes back undoing a move, that has to be legal and checked */
  void back(const Move& lastMove,
            const Move& move,
            const StatePtr& pos);

  /** Warps to a new position */
  void warp(const Move& lastMove,
            const StatePtr& pos);

  /** Adjusts a sprite to the correct position */
  void adjustSprite(const Point& p);

  /** Sets the current turn */
  void setTurn(const IColor* turn);
  
  

  // TAGUA API
  Components* components() const;
  
  virtual AnimationPtr group(const QString& flags) const;
  virtual AnimationPtr appear(const NamedSprite& sprite, 
                              const QString& flags) const;
  virtual AnimationPtr disappear(const NamedSprite& sprite, 
                                 const QString& flags) const;
  virtual AnimationPtr move(const NamedSprite& sprite, 
                            const Point& destination,
                            const QString& flags) const;
  virtual AnimationPtr move(const NamedSprite& sprite,
                            int pool,
                            int destination,
                            const QString& flags) const;
  virtual AnimationPtr morph(const NamedSprite& sprite1, 
                             const NamedSprite& sprite2, 
                             const QString& flags) const;
};


#endif //GRAPHICALSYSTEM_H
