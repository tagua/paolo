/*
  Copyright (c) 2006 Paolo Capriotti <p.capriotti@gmail.com>
            (c) 2006 Maurizio Monge <maurizio.monge@kdemail.net>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
*/

#ifndef EXAMINATIONENTITY_H
#define EXAMINATIONENTITY_H

#include "agent.h"
#include "agentgroup.h"
#include "userentity.h"
#include "icsapi_fwd.h"
#include "icslistener.h"

class Components;
class ICSConnection;

class ExaminationEntity : public UserEntity
                        , public ICSListener {
  Components* m_components;
  ICSAPIPtr m_icsapi;
  
  int m_game_number;
  boost::shared_ptr<ICSConnection> m_connection;
  AgentGroupDispatcher m_dispatcher;
public:
  ExaminationEntity(Components*, const boost::shared_ptr<Game>&,
                    int, const boost::shared_ptr<ICSConnection>&, AgentGroup*);

  virtual QString save() const;
  virtual void loadPGN(const PGN&);

  virtual bool testMove(Move&) const;
  virtual bool testPremove(const Move&) const;
  virtual void executeMove(const Move&);
  virtual void addPremove(const Move&);
  virtual void cancelPremove();
  virtual InteractionType validTurn(const Point&) const;
  virtual InteractionType validTurn(const IColor*) const;
  virtual bool movable(const Point&) const;
  virtual bool jump(const Index&);
  virtual bool gotoFirst();
  virtual bool gotoLast();
  virtual bool goTo(const Index&);
  virtual bool forward();
  virtual bool back();
  virtual bool undo();
  virtual bool redo();
  virtual bool truncate();
  virtual bool promoteVariation();

  virtual void notifyStyle12(const PositionInfo&, bool is_starting);
  virtual void notifyPool(const PoolInfo&);
  virtual void notifyMoveList(int, const StatePtr&, const PGN&);

  virtual void notifyClockUpdate(int, int) { }
  virtual void notifyMove(const Index&);
  virtual void notifyBack();
  virtual void notifyForward();
  virtual void notifyGotoFirst();
  virtual void notifyGotoLast();

  virtual bool attach();
};

#endif // EXAMINATIONENTITY_H
