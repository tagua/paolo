/*
  Copyright (c) 2006 Paolo Capriotti <p.capriotti@gmail.com>
            (c) 2006 Maurizio Monge <maurizio.monge@kdemail.net>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
*/

#ifndef ICSENTITY_H
#define ICSENTITY_H

#include <core/turnpolicy.h>
#include "entity.h"
#include "agent.h"
#include "agentgroup.h"
#include "icsapi_fwd.h"
#include "icslistener.h"

class Components;
class IColor;
class IMoveSerializer;
class IValidator;
class ICSConnection;
class Move;

class ICSEntity : public Entity
                , public Agent
                , public ICSListener {
protected:
  Components* m_components;
  ICSAPIPtr m_icsapi;
  
  boost::shared_ptr<ICSConnection> m_connection;
  const IColor* m_side;
  int m_game_number;
  bool m_editing_mode;
  AgentGroupDispatcher m_dispatcher;

  void updateGame(const Index& index, Move& icsMove, const StatePtr& icsPos);

  enum UpdateType {
    MoveForward,
    MoveBack,
    Forward,
    Back,
    NoMove,
    NonComparableIndexes
  };

  UpdateType getUpdate(const Index& index);
public:
  ICSEntity(Components*, const boost::shared_ptr<Game>&,
           const IColor*, int,
           const boost::shared_ptr<ICSConnection>&, AgentGroup*);

  virtual void executeMove(const Move&);
  virtual void notifyStyle12(const PositionInfo&, bool is_starting);
  virtual void notifyPool(const PoolInfo&);
  virtual void notifyMoveList(int, const StatePtr&, const PGN&);

  virtual StatePtr position() const;

  virtual void notifyClockUpdate(int, int) { }
  virtual void notifyMove(const Index&);
  virtual void notifyBack() { }
  virtual void notifyForward() { }
  virtual void notifyGotoFirst() { }
  virtual void notifyGotoLast() { }

  void requestMoves();

  virtual bool canDetach() const;
  virtual bool attach();

  bool canEdit() const;
  bool canEdit(const Index& index) const;
  void setupTurnTest(TurnTest& test) const;
};

class ObservingEntity : public ICSEntity {
protected:
  bool m_attached;
public:
  ObservingEntity(Components*, const boost::shared_ptr<Game>&,
                  int gameNumber, const boost::shared_ptr<ICSConnection>&, AgentGroup*);
  ~ObservingEntity();

  virtual void detach();
};

#endif // ICSENTITY_H
