/*
  Copyright (c) 2006 Paolo Capriotti <p.capriotti@gmail.com>
            (c) 2006 Maurizio Monge <maurizio.monge@kdemail.net>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
*/

#include "variants.h"
#include <memory>
#include <KDebug>
#include "foreach.h"
#include "variant.h"

void load_plugins();

typedef Repository* PluginFactory(IVariantLoader*);

Repository* Variants::load(const KService::Ptr& plugin) {
    // load plugin
  QString symbol = plugin->library() + "_initrepo";
  KLibrary pluginLib(plugin->library());
  
  // resolve initrepo function
  void* initrepo_ = pluginLib.resolveSymbol(qPrintable(symbol));
  if (!initrepo_) {
    kWarning() << "Error loading plugin" << plugin->library();
    kWarning() << pluginLib.errorString();
    return false;
  }
  PluginFactory* initrepo = reinterpret_cast<PluginFactory*>(initrepo_);
  
  return (*initrepo)(this);
}

Variants::Variants() { }

Variants& Variants::self() {
  static Variants inst;
  return inst;
}

Variant* Variants::create(const KService::Ptr& plugin) {
  Repository* repo = load(plugin);
  if (repo)
    return new Variant(plugin->name(), repo,
      plugin->property("X-Tagua-Proxy").toString());
      
  return 0;
}

Variant* Variants::create(const QString& name) {
  KService::Ptr service = get(name);
  return service ? create(service) : 0;
}

KService::Ptr Variants::get(const QString& name) {
  KService::List services = 
    KServiceTypeTrader::self()->query("Tagua/Variant", 
                                      "Name =~ '" + name + "'");
  if (services.empty())
    return KService::Ptr();
  else
    return services[0];
}

KService::List Variants::all() const {
  return KServiceTypeTrader::self()->query(
    "Tagua/Variant", "[X-Tagua-Hidden] == false");
}

Repository* Variants::getRepository(const QString& variant) {
  KService::Ptr service = get(variant);
  if (!service) return 0;
  
  return load(service);
}
