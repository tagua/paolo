/*
  Copyright (c) 2007 Paolo Capriotti <p.capriotti@gmail.com>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
*/

#ifndef CORE__ANIMATOR_H
#define CORE__ANIMATOR_H

#include "animation_fwd.h"
#include "export.h"

class INamer;
class IState;
class Move;
class TaguaAPI;

/**
  * @brief A Warper defines graphical transition between unrelated states.
  */
class TAGUA_EXPORT IWarper {
public:
  virtual ~IWarper();
      
  /**
    * Instantly update the graphical position using the given state.
    */
  virtual AnimationPtr warp(const IState* state) = 0;  
};

/**
  * @brief An Animator defines graphical transition between two game states.
  *
  * An animator is a class which is given a difference between a graphical 
  * position and a game state, and schedules an animation which performs the
  * update.
  * 
  * If the difference is due to a move, the animator tries to interpret it
  * following the rules of the game to get to the final state.
  * 
  * If the process does not succeed exactly (i.e. if the updated graphical
  * position does not equal the given final state), the animator is required
  * to force equality using a direct update.
  */
class TAGUA_EXPORT IAnimator : public IWarper {
public:
  virtual ~IAnimator();

  /**
    * Animate forward syncing to the given state.
    */
  virtual AnimationPtr forward(const Move& move, const IState* state) = 0;
  
  /**
    * Animate backward syncing to the given state.
    */
  virtual AnimationPtr back(const Move& move, const IState* state) = 0;
  
  /**
    * Set the warper object.
    */
  virtual void setWarper(IWarper* warper) = 0;
};

class TAGUA_EXPORT IAnimatorFactory {
public:
  virtual ~IAnimatorFactory();
  
  virtual IAnimator* create(TaguaAPI* api, const INamer* namer) const = 0;
};

#endif // CORE__ANIMATOR_H

