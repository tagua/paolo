/*
  Copyright (c) 2007 Paolo Capriotti <p.capriotti@gmail.com>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
*/

#ifndef POOLCOLLECTION_FWD_H
#define POOLCOLLECTION_FWD_H

#include <boost/shared_ptr.hpp>

class IPoolCollection;
typedef boost::shared_ptr<IPoolCollection> PoolCollectionPtr;

#endif // POOLCOLLECTION_FWD_H
