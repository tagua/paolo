/*
  Copyright (c) 2007 Paolo Capriotti <p.capriotti@gmail.com>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
*/

#ifndef CORE__ANIMATION_FWD_H
#define CORE__ANIMATION_FWD_H

#include <boost/shared_ptr.hpp>

class Animation;
typedef boost::shared_ptr<Animation> AnimationPtr;

#endif // CORE__ANIMATION_FWD_H

