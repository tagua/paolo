/*
  Copyright (c) 2007 Paolo Capriotti <p.capriotti@gmail.com>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
*/

#include "animation.h"

#include "dropanimator.h"
#include "move.h"
#include "namer.h"
#include "piece.h"
#include "pool.h"
#include "poolcollection.h"
#include "state.h"
#include "taguaapi.h"

DropAnimator::~DropAnimator() { delete m_base; }

DropAnimator::DropAnimator(IAnimator* base, 
                           TaguaAPI* api,
                           const INamer* namer,
                           const IColor** players)
: BaseAnimator(api, namer)
, m_base(base)
, m_players(players) {
  m_warper = this;
}

AnimationPtr DropAnimator::warp(const IState* state) {
  updatePool(state);
  return BaseAnimator::warp(state);
}

AnimationPtr DropAnimator::forward(const Move& move, const IState* state) {
  if (move.drop() != Piece()) {
    AnimationPtr res = m_api->group("default");

    NamedSprite captured = m_api->takeSprite(move.dst());
  
    std::pair<const IColor*, int> dropped = m_api->droppedPoolPiece();
    if (dropped.first && dropped.second != -1 &&
        m_api->state()->pools()->
          pool(dropped.first)->get(dropped.second) == move.drop()) {
      NamedSprite drop = m_api->takePoolSprite(dropped.first, dropped.second);
      m_api->setSprite(move.dst(), drop);
      res->addPreAnimation(m_api->move(drop, move.dst(), "default"));
    }
    else {      
      NamedSprite drop = m_api->setPiece(move.dst(), move.drop(), false);
      res->addPreAnimation(m_api->appear(drop, "default"));
    }

    if (captured)
      res->addPostAnimation(m_api->disappear(captured, "destroy"));
    res->addPostAnimation(m_warper->warp(state));
    
    return res;
  }
  else {
    return m_base->forward(move, state);
  }
}

AnimationPtr DropAnimator::back(const Move& move, const IState* state) {
  if (move.drop() != Piece()) {
    AnimationPtr res = m_api->group("default");
    NamedSprite drop = m_api->takeSprite(move.dst());
    res->addPostAnimation(m_api->disappear(drop, "default"));
    
    res->addPostAnimation(m_warper->warp(state));
    return res;
  }
  
  return m_base->back(move, state);
}

void DropAnimator::updatePool(const IState* final) {
  for (int color = 0; color < 2; color++) {
    const IColor* c = m_players[color];
    const IPool* pool = final->pools()->pool(c);
    const int n = pool->size();
    
    for (int index = 0; index < n; ) {
      // precondition: pool and graphical pool match up to index
    
      // no more sprites on the graphical pool
      if (index >= m_api->poolSize(color)) {
        // add extra sprites
        for (int i = index; i < n; i++)
          m_api->insertPoolPiece(c, i, pool->get(i));
        
        // done
        break;
      }
      
      NamedSprite sprite = m_api->getPoolSprite(c, index);
      int i;
      
      // find a matching piece on the pool
      for (i = index; i < n; i++) {
        if (m_namer->name(pool->get(i)) == sprite.name())
          break;
      }
      
      if (i < n) {
        // matching piece found on the pool
        // insert all pieces before this one on the graphical pool
        for (int j = index; j < i; j++)
          m_api->insertPoolPiece(c, j, pool->get(j));
        index = i + 1;
      }
      else {
        // no such piece found: remove it from the graphical pool
        m_api->removePoolSprite(c, index);
      }
    }
    
    // remove extra pieces from the graphical pool
    while (m_api->poolSize(color) > n)
      m_api->removePoolSprite(c, n);
  }
}

void DropAnimator::setWarper(IWarper* warper) { m_warper = warper; }

