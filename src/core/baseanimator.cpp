/*
  Copyright (c) 2007 Paolo Capriotti <p.capriotti@gmail.com>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
*/

#include "baseanimator.h"

#include "animation.h"
#include "board.h"
#include "namer.h"
#include "piece.h"
#include "state.h"
#include "taguaapi.h"

BaseAnimator::BaseAnimator(TaguaAPI* api, const INamer* namer)
: m_api(api)
, m_namer(namer) { }

AnimationPtr BaseAnimator::warp(const IState* state) {
  const IState* current = m_api->state();
  AnimationPtr res = m_api->group("default");
  
  for (int i = 0; i < current->board()->size().x; i++) {
    for (int j = 0; j < current->board()->size().y; j++) {
      Point p(i, j);  
      
      NamedSprite sprite = m_api->getSprite(p);
      Piece piece = state->board()->get(p);
      AnimationPtr anim;
    
      if (!sprite && piece != Piece()) {
        anim = m_api->appear(m_api->setPiece(p, piece, false), "instant");
      }
      
      else if (sprite && piece == Piece()) {
        m_api->takeSprite(p);
        anim = m_api->disappear(sprite, "instant");
      }
      
      // sprite and piece differ
      else if (sprite && piece != Piece() && 
               m_namer->name(piece) != sprite.name()) {
        m_api->takeSprite(p);
        anim = m_api->morph(sprite, m_api->setPiece(p, piece, false), "instant");
      }
      
      res->addPreAnimation(anim);
    }
  }
  
  return res;
}

AnimationPtr BaseAnimator::forward(const Move&, const IState*) {
  return AnimationPtr();
}

AnimationPtr BaseAnimator::back(const Move&, const IState*) {
  return AnimationPtr();
}


