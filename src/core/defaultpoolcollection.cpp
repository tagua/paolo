/*
  Copyright (c) 2007 Paolo Capriotti <p.capriotti@gmail.com>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
*/

#include "defaultpoolcollection.h"
#include "pool.h"
#include "taguacast.h"

DefaultPoolCollection::DefaultPoolCollection() { }

DefaultPoolCollection::~DefaultPoolCollection() {
  for (PoolMap::const_iterator it = m_pools.begin(),
       end = m_pools.end(); it != end; ++it) {
    delete it.value();
  }
}

IPoolCollection* DefaultPoolCollection::clone() const {
  DefaultPoolCollection* c = new DefaultPoolCollection;
  
  for (PoolMap::const_iterator it = m_pools.begin(),
       end = m_pools.end(); it != end; ++it) {
    c->m_pools.insert(it.key(), it.value()->clone());
  }
  
  return c;
}

bool DefaultPoolCollection::equals(const IPoolCollection* other_) const {
  PoolMap::const_iterator it1 = m_pools.begin();
  PoolMap::const_iterator end1 = m_pools.end();
  TAGUA_CAST(other, const DefaultPoolCollection, false);
  PoolMap::const_iterator it2 = other->m_pools.begin();
  PoolMap::const_iterator end2 = other->m_pools.end();
  
  while (it1 != end1 || it2 != end2) {
    if (it2 == end2 || it1.key() < it2.key()) {
      if (!it1.value()->empty()) return false;
      ++it1;
    }
    else if (it1 == end1 || it1.key() > it1.key()) {
      if (!it2.value()->empty()) return false;
      ++it2;
    }
    else {
      if (!it1.value()->equals(it2.value())) return false;
      ++it1;
      ++it2;
    }
  }
  
  return true;
}

IPool* DefaultPoolCollection::pool(const IColor* player) {
  return m_pools.value(player);
}

const IPool* DefaultPoolCollection::pool(const IColor* player) const {
  return m_pools.value(player);
}

void DefaultPoolCollection::addPool(const IColor* color, IPool* pool) {
  m_pools.insert(color, pool);
}
