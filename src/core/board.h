/*
  Copyright (c) 2007 Paolo Capriotti <p.capriotti@gmail.com>
            (c) 2007 Maurizio Monge <maurizio.monge@kdemail.net>
            
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
*/

#ifndef CORE__BOARD_H
#define CORE__BOARD_H

#include <QStringList>
#include <vector>

#include "point.h"
#include "pathinfo.h"

class Piece;

// TODO: make Board a component

/**
  * @brief A grid of pieces.
  * 
  * The board class maintains a rectangular grid of Piece objects,
  * providing methods to access, add, remove and find them.
  */
class TAGUA_EXPORT Board {
  Point m_size;
  std::vector<Piece> m_data;
public:
  /**
    * Create a new Board with the given size.
    */
  explicit Board(const Point& size);
  
  /**
    * Create a Board copying a given one.
    */
  explicit Board(const Board* other);
  
  /**
    * Compare two boards for equality.
    */
  bool equals(const Board* other) const;
  
  /**
    * @return Board size.
    */
  Point size() const;
  
  /**
    * Retrieve a piece from the board.
    * @param p Coordinates of the piece.
    * @return The piece at that square, or an invalid piece, if the square is out of the board.
    */
  Piece get(const Point& p) const;
  
  /**
    * Add a piece to the board.
    * @param p Coordinates of the piece.
    * @param piece The piece to add.
    * @note This function does nothing if @a p is out of the board.
    */
  void set(const Point& p, const Piece& piece);
  
  /**
    * @return Whether @a p is a valid board square.
    */
  bool valid(const Point& p) const;
  
  /**
    * @return Information on the path joining @a from and @a to.
    */
  PathInfo path(const Point& from, const Point& to) const;
  
  /**
    * Searches the board for a given piece.
    * @return The first square where the piece is found, or an
    *         invalid point, if no such piece exists on the board.
    */
  Point find(const Piece& piece) const;
  
  /**
    * Coordinates displayed at the board border.
    */
  QStringList borderCoords() const;
};

#endif // CORE__BOARD_H
