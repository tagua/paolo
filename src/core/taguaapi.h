/*
  Copyright (c) 2006 Paolo Capriotti <p.capriotti@gmail.com>
            (c) 2006 Maurizio Monge <maurizio.monge@kdemail.net>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
*/

#ifndef CORE__TAGUAAPI_H
#define CORE__TAGUAAPI_H

#include <vector>
#include <boost/shared_ptr.hpp>
#include "animation_fwd.h"
#include "namedsprite.h"
#include "point.h"

typedef boost::shared_ptr<class Sprite> SpritePtr;
class IColor;
class IState;
class Piece;

/**
  * This class defines the interface that will be used by the animator to modify
  * tagua graphics.
  */
class TAGUA_EXPORT TaguaAPI {
public:
  virtual ~TaguaAPI();

  /**
    * @return The current game state
    */
  virtual const IState* state() const = 0;

  /**
    * @return a sprite at the position \a index in the graphical pool.
    */
  virtual NamedSprite getSprite(const Point& p) = 0;

  /**
    * Removes a sprite at the position \a index in the graphical pool.
    * @return the newly created sprite.
    */
  virtual NamedSprite takeSprite(const Point& p) = 0;

  /**
    * Sets the piece at the position @a index in the graphical pool.
    * @return the newly created sprite.
    */
  virtual NamedSprite setPiece(const Point& p, const Piece& piece, bool show) = 0;

  /**
    *  Create a new piece, but do not add it to the graphical system.
    * @return the newly created sprite.
    */
  virtual NamedSprite createPiece(const Point& p, const Piece& piece,  bool show) = 0;

  /**
    * Sets the sprite at the position @a index in the graphical pool.
    * @return the newly created sprite.
    */
  virtual void setSprite(const Point& p, const NamedSprite& sprite) = 0;

  /**
    * @return how many sprites are contained in the pool
    */
  virtual int poolSize(int pool) = 0;

  /**
    * @return the sprite at the position @a index in the graphical pool.
    */
  virtual NamedSprite getPoolSprite(const IColor* pool, int index) = 0;

  /**
    * Removes the sprite at the position @a index in the graphical pool.
    */
  virtual void removePoolSprite(const IColor* pool, int index) = 0;

  /**
    * Removes the sprite at the position @a index in the graphical pool (only for drops).
    * @return the removed sprite.
    */
  virtual NamedSprite takePoolSprite(const IColor* pool, int index) = 0;

  /**
    * Inserts a sprite at the position @a index in the graphical pool.
    * @return the newly created sprite.
    */
  virtual NamedSprite insertPoolPiece(const IColor* pool, int index, const Piece& piece) = 0;

  /**
    * @return the piece of the pool that has been dropped, or (0,-1)
    */
  virtual std::pair<const IColor*, int> droppedPoolPiece() = 0;

  virtual AnimationPtr group(const QString& flags) const = 0;
  virtual AnimationPtr appear(const NamedSprite& sprite, 
                              const QString& flags) const = 0;
  virtual AnimationPtr disappear(const NamedSprite& sprite, 
                                 const QString& flags) const = 0;
  virtual AnimationPtr move(const NamedSprite& sprite, 
                            const Point& destination,
                            const QString& flags) const = 0;
  virtual AnimationPtr move(const NamedSprite& sprite,
                            int pool,
                            int destination,
                            const QString& flags) const = 0;
  virtual AnimationPtr morph(const NamedSprite& sprite1, 
                             const NamedSprite& sprite2, 
                             const QString& flags) const = 0;  
};

#endif // CORE__TAGUAAPI_H
