/*
  Copyright (c) 2007 Paolo Capriotti <p.capriotti@gmail.com>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
*/

#ifndef DELEGATORS__BEHAVIOUR_H
#define DELEGATORS__BEHAVIOUR_H

#include "../behaviour.h"

namespace Delegators {

class Behaviour : public IBehaviour {
protected:
  const IBehaviour* m_behaviour;
public:
  Behaviour(const IBehaviour* behaviour) : m_behaviour(behaviour) { }
  virtual ~Behaviour() { delete m_behaviour; }
  
  virtual void captureOn(IState* state, const Point& square) const {
    m_behaviour->captureOn(state, square);
  }
  virtual void move(IState* state, const Move& m) const { m_behaviour->move(state, m); }
  virtual void advanceTurn(IState* state) const { m_behaviour->advanceTurn(state); }
  virtual Point captureSquare(const IState* state, const Move& m) const {
    return m_behaviour->captureSquare(state, m);
  }
  virtual const IColor* opponent(const IColor* player) const {
    return m_behaviour->opponent(player);
  }
  virtual Point direction(const IColor* player) const { 
    return m_behaviour->direction(player);
  }
};

}

#endif // DELEGATORS__BEHAVIOUR_H


