#ifndef CORE__REPOSITORY_H
#define CORE__REPOSITORY_H

#include <QMap>
#include <QString>
#include "component.h"
#include "export.h"

/**
  * @brief A hierarchical collection of component defining a variant.
  * 
  * Components of a variant are maintained in a hierarchical structure 
  * called repository. All components have a path that uniquely identifies 
  * them inside a given variant.
  * 
  * Non-leaf nodes are not actual components, and are called namespaces. 
  * Namespaces can be nested.
  * 
  * Since the number of components inside a repository is not likely to
  * grow over a dozen or so, there is not practical need for this structure
  * to reflect its hierarchical representation, so we simply maintain all 
  * components inside a path -> component association.
  */
class TAGUA_EXPORT Repository {
public:
  typedef QMap<QString, ComponentPtr> ComponentMap;
private:
  ComponentMap m_components;
  Repository* m_proxy;
public:
  Repository();
  
  /**
    * Add a new component to the repository at the specified path.
    * If a component already exists at that path, it will be silently
    * overwritten.
    */
  void addComponent(const QString& path, ComponentPtr component);
  
  /**
    * Retrieve a component from a given path.
    * @return The component at path @a path, or a null pointer if
    *         no component exists at that path.
    */
  ComponentPtr getComponent(const QString& path) const;
  
  /**
    * Remove a component from a given path. Do nothing if no
    * component exists at that path.
    * @return The component removed from path @a path.
    */
  ComponentPtr takeComponent(const QString& path);
  
  /**
    * Determine the list of components living inside a given namespace.
    * Nested namespaces will be silently ignored.
    */
  ComponentMap listComponents(QString path) const;
  
  /**
    * Add a proxy repository, that will be queried when a component
    * is not found here.
    */
  void setProxy(Repository* proxy);
};

// template <typename Interface>
// Interface* requestInterface(Component* component) {
//   Interface* iface = dynamic_cast<Interface*>(component);
//   if (iface)
//     return iface;
//   else
//     return new typename Interface::Adaptor(iface);
// }

template <typename Interface>
Interface* requestInterface(Component* component) {
  return dynamic_cast<Interface*>(component);
}

#endif // CORE__REPOSITORY_H

