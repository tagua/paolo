/*
  Copyright (c) 2007 Paolo Capriotti <p.capriotti@gmail.com>
            (c) 2007 Maurizio Monge <maurizio.monge@kdemail.net>
            
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
*/

#ifndef CORE__MOVE_SERIALIZER_H
#define CORE__MOVE_SERIALIZER_H

#include <QString>
#include "export.h"

class IState;
class IType;
class Move;

/**
  * @brief MoveSerializer takes care of converting moves to a displayable form and vice-versa.
  */
class TAGUA_EXPORT IMoveSerializer {
public:
  virtual ~IMoveSerializer();
  
  /**
    * Serialize a move given a reference game state.
    * It is assumed that the move has already been tested against @a ref.
    * Calling this function on an untested or illegal move is safe but its return
    * value is undefined.
    * Do not try to call this function on an invalid move.
    * @param ref The position in which this move shall be executed.
    * @return A string representation for this move.
    */
  virtual QString serialize(const Move& move, const IState* ref) const = 0;
  
  /**
    * Convert a string representation of a move back to a move object.
    * @param ref The position in which this move shall be executed.
    * @return A move corresponding to the given string representation.
    */
  virtual Move deserialize(const QString& str, const IState* ref) const = 0;
  
  /**
    * @return The symbol associated to the given type.
    */
  virtual QString symbol(const IType* type) const = 0;
  
  /**
    * Compute a move suffix (e.g. to represent checkmate).
    * @param ref The position in which this move shall be executed.
    * @return A suffix for the given move.
    */
  virtual QString suffix(const Move& move, const IState* ref) const = 0;
  
  /**
    * @return Serializer type.
    */
  virtual QString type() const = 0;
  
  virtual void setDelegator(IMoveSerializer* delegator) = 0;
};

#endif // CORE__MOVE_SERIALIZER_H

