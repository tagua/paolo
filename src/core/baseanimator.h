/*
  Copyright (c) 2007 Paolo Capriotti <p.capriotti@gmail.com>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
*/

#ifndef CORE__BASEANIMATOR_H
#define CORE__BASEANIMATOR_H

#include "component.h"
#include "animation_fwd.h"
#include "animator.h"

class INamer;
class TaguaAPI;

class TAGUA_EXPORT BaseAnimator : public Component, public IAnimator {
protected:
  TaguaAPI* m_api;
  const INamer* m_namer;
public:
  BaseAnimator(TaguaAPI* api, const INamer* namer);

  virtual AnimationPtr warp(const IState* state);
  virtual AnimationPtr forward(const Move& move, const IState* state);
  virtual AnimationPtr back(const Move& move, const IState* state);
};

#endif // CORE__BASEANIMATOR_H

