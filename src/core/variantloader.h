/*
  Copyright (c) 2007 Paolo Capriotti <p.capriotti@gmail.com>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
*/

#ifndef VARIANTLOADER_H
#define VARIANTLOADER_H

#include <QString>
#include "export.h"

class Repository;

class TAGUA_EXPORT IVariantLoader {
public:
  virtual ~IVariantLoader();
  
  virtual Repository* getRepository(const QString& variant) = 0;
};

#endif // VARIANTLOADER_H
