/*
  Copyright (c) 2007 Paolo Capriotti <p.capriotti@gmail.com>
            (c) 2007 Maurizio Monge <maurizio.monge@kdemail.net>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
*/

#include "repository.h"

Repository::Repository()
: m_proxy(0) { }

void Repository::addComponent(const QString& path, ComponentPtr component) {
  m_components[path] = component;
}

ComponentPtr Repository::getComponent(const QString& path) const {
  ComponentPtr res = m_components.value(path);
  if (!res && m_proxy) 
    res = m_proxy->getComponent(path);
  return res;
}

ComponentPtr Repository::takeComponent(const QString& path) {
  return m_components.take(path);
}

Repository::ComponentMap Repository::listComponents(QString path) const {
  if (!path.endsWith("/")) path += "/";
  
  ComponentMap res;
  ComponentMap::const_iterator it = m_components.lowerBound(path);
  ComponentMap::const_iterator end = m_components.end();
  
  for ( ; it != end; ++it) {
    if (it.key().startsWith(path))
      res[it.key().mid(path.size())] = it.value();
    else
      break;
  }
  
  if (m_proxy)
    res = res.unite(m_proxy->listComponents(path));
  
  return res;
}

void Repository::setProxy(Repository* proxy) {
  m_proxy = proxy;
}
