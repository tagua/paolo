/*
  Copyright (c) 2007 Paolo Capriotti <p.capriotti@gmail.com>
            (c) 2007 Maurizio Monge <maurizio.monge@kdemail.net>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
*/

#ifndef CORE__COMPONENT_H
#define CORE__COMPONENT_H

#include "component_fwd.h"
#include "export.h"
#include <QObject>

/**
  * @brief A Component is a reusable unit in the description of a Tagua variant.
  */
class TAGUA_EXPORT Component : public QObject {
Q_OBJECT
public:
  Component();
};

#endif // CORE__COMPONENT_H
