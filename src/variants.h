/*
  Copyright (c) 2006 Paolo Capriotti <p.capriotti@gmail.com>
            (c) 2006 Maurizio Monge <maurizio.monge@kdemail.net>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
*/


#ifndef VARIANTS_H
#define VARIANTS_H

#include <QStringList>
#include <KService>
#include <KServiceTypeTrader>
#include "core/variantloader.h"

class Variant;
class Repository;

class Variants : public IVariantLoader {
  Variants();
  
  Repository* load(const KService::Ptr& plugin);
public:
  KService::Ptr get(const QString& name);
  Variant* create(const KService::Ptr& plugin);
  Variant* create(const QString& name);
  
  /**
    * \return a list of all non-hidden variants.
    */
  KService::List all() const;
  
  static Variants& self();
  
  virtual Repository* getRepository(const QString& variant);
};

#endif // VARIANTS_H
