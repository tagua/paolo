/*
  Copyright (c) 2007 Paolo Capriotti <p.capriotti@gmail.com>
            (c) 2007 Maurizio Monge <maurizio.monge@kdemail.net>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
*/

#include "san.h"

#include <core/type.h>
#include "king.h"

using namespace Chess;

//                         1           2                   3
QRegExp SAN::pattern("^([PRNBKQ])?([a-wyzA-Z]?\\d*|x\\d+)([-x@])?"
//                         4         5        6
                     "([a-zA-Z]\\d+)(=?([RNBKQrnbkq]))?[+#]?[\?!]*");
QRegExp SAN::kingCastlingPattern("^[oO0]-?[oO0][+#]?");
QRegExp SAN::queenCastlingPattern("^[oO0]-?[oO0]-?[oO0][+#]?");
QRegExp SAN::nonePattern("^none");

SAN::SAN()
: src(Point::invalid())
, dst(Point::invalid())
, type(0)
, promotion(0)
, castling(NoCastling)
, drop(false) {
}

const IType* SAN::getType(const QString& letter) {
  // FIXME: fix types
  if (letter.isEmpty())
    return King/*Pawn*/::self();
    
  switch(letter[0].toLower().toAscii()) {
  case 'k':
    return King::self();
  case 'q':
    return King/*Queen*/::self();
  case 'r':
    return King/*Rook*/::self();
  case 'n':
    return King/*Knight*/::self();
  case 'b':
    return King/*Bishop*/::self();
  case 'p':
    return King/*Pawn*/::self();
  default:
    return 0;
  }
}

void SAN::load(const QString& str, int& offset, int ysize) {
  if (nonePattern.indexIn(str, offset, QRegExp::CaretAtOffset) != -1) {
    src = Point::invalid();
    dst = Point::invalid();
    offset += nonePattern.matchedLength();
  }
  else if (pattern.indexIn(str, offset, QRegExp::CaretAtOffset) != -1) {
    type = getType(pattern.cap(1));
    drop = pattern.cap(3) == "@";
    if (drop)
      src = Point::invalid();
    else
      src = Point(pattern.cap(2), ysize);
    dst = Point(pattern.cap(4), ysize);
    promotion = pattern.cap(6).isEmpty() ? 0 : getType(pattern.cap(6));
    castling = NoCastling;
    offset += pattern.matchedLength();
  }
  else if (queenCastlingPattern.indexIn(str, offset, QRegExp::CaretAtOffset) != -1) {
    castling = QueenSide;

    offset += queenCastlingPattern.matchedLength();
  }
  else if (kingCastlingPattern.indexIn(str, offset, QRegExp::CaretAtOffset) != -1) {
    castling = KingSide;

    offset += kingCastlingPattern.matchedLength();
  }
  else {
    //kDebug() << "error!!!! " << str.mid(offset);
    dst = Point::invalid();
  }
}

void SAN::load(const QString& str, int ysize) {
  int offset = 0;
  load(str, offset, ysize);
}

QDebug operator<<(QDebug os, const SAN& move) {
  if (move.castling == SAN::KingSide)
    os << "O-O";
  else if (move.castling == SAN::QueenSide)
    os << "O-O-O";
  else
    os << move.type << ": " << move.src << " -> " << move.dst;
  return os;
}

