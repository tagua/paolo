#include "validator.h"
#include <memory>
#include <core/behaviour.h>
#include <core/board.h>
#include <core/move.h>
#include <core/state.h>
#include <core/type.h>
#include "colors.h"
#include "types.h"

namespace Chess {

Validator::Validator() : m_delegator(this) { }

bool Validator::pseudolegal(const IState* state, Move& move) const {
  if (!state->board()->valid(move.src())) return false;
  if (!state->board()->valid(move.dst())) return false;
  
  Piece piece = state->board()->get(move.src());
  if (piece == Piece()) return false;
  
  const IBehaviour* behaviour = state->behaviour();
  if (!behaviour) return false;
  
  const IColor* thisTurn = piece.color();
  const IColor* otherTurn = behaviour->opponent(thisTurn);
  
  if (state->turn() != thisTurn) return false;
  
  Piece target = state->board()->get(move.dst());
  if (target.color() == piece.color())
    return false;
  if (!piece.type()->canMove(piece, target, move, state))
    return false;

  if (move.type() == "promotion") {
    const IType* promotionType = move.promotion();
    if (promotionType != Queen::self() &&
        promotionType != Bishop::self() &&
        promotionType != Rook::self() &&
        promotionType != Knight::self()) return false;
  }
  
  if (move.type() == "king_side_castling") {
    if (m_delegator->attacks(state, otherTurn, move.src()) ||
        m_delegator->attacks(state, otherTurn, move.src() + Point(1, 0), piece))
        return false;
  }
  if (move.type() == "queen_side_castling") {
    if (m_delegator->attacks(state, otherTurn, move.src()) ||
        m_delegator->attacks(state, otherTurn, move.src() + Point(-1, 0), piece))
        return false;
  }
  
  return true;
}

bool Validator::legal(const IState* state, Move& move) const {
  // do not check a move more than once
  if (!move.type().isEmpty()) return true;

  if (!m_delegator->pseudolegal(state, move))
    return false;
    
  const IBehaviour* behaviour = state->behaviour();
  if (!behaviour) return false;
    
  const IColor* turn = m_delegator->mover(state, move);
  
  std::auto_ptr<IState> tmp(state->clone());
  tmp->move(move);
  
  Point kingPos = tmp->board()->find(Piece(turn, King::self()));
  
  if (kingPos == Point::invalid())
    return false;
    
  if (m_delegator->attacks(tmp.get(), behaviour->opponent(turn), kingPos))
    return false;
    
  // set move type as normal, if not already set    
  if (move.type().isEmpty()) move.setType("normal");
  return true;
}

bool Validator::attacks(const IState* state, const IColor* player, 
                        const Point& square, const Piece& target_) const {
  Piece target;
  if (target_ != Piece())
    target = target_;
  else
    target = state->board()->get(square);

  for (int i = 0; i < state->board()->size().x; i++) {
    for (int j = 0; j < state->board()->size().y; j++) {
      Point p(i, j);
      Piece piece = state->board()->get(p);
      Move move(p, square);
      if (piece != Piece() && 
          piece.color() == player &&
          piece.type()->canMove(piece, target, move, state))
        return true;
    }
  }
  return false;
}

const IColor* Validator::mover(const IState* state, const Move& move) const {
  return state->board()->get(move.src()).color();
}

void Validator::setDelegator(IValidator* delegator) {
  m_delegator = delegator;
}

}

