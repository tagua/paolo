/*
  Copyright (c) 2007 Paolo Capriotti <p.capriotti@gmail.com>
            (c) 2007 Maurizio Monge <maurizio.monge@kdemail.net>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
*/

#include <core/defaultpolicy.h>
#include <core/repository.h>
#include <core/variantloader.h>

#include "animator.h"
#include "behaviour.h"
#include "castlingrules.h"
#include "colors.h"
#include "moveserializer.h"
#include "namer.h"
#include "types.h"
#include "state.h"
#include "validator.h"

using namespace Chess;

extern "C" KDE_EXPORT Repository* taguachess_initrepo(IVariantLoader*) {
  Repository* repo = new Repository;
  
  repo->addComponent("player/0", dynamic_cast<Component*>(COLORS[0]));
  repo->addComponent("player/1", dynamic_cast<Component*>(COLORS[1]));
  
  repo->addComponent("type/king", King::self());
  repo->addComponent("type/queen", Queen::self());
  repo->addComponent("type/rook", Rook::self());
  repo->addComponent("type/bishop", Bishop::self());
  repo->addComponent("type/knight", Knight::self());
  repo->addComponent("type/pawn", Pawn::self());
  
  CastlingRules* castling_rules = new CastlingRules;
  repo->addComponent("extra/castling_rules", castling_rules);
  
  repo->addComponent("state", new State(new Behaviour, castling_rules, Point(8, 8)));
  Validator* validator = new Validator;
  repo->addComponent("validator", validator);
  repo->addComponent("animator_factory", new AnimatorFactory);
  repo->addComponent("namer", new Namer);
  repo->addComponent("policy", new DefaultPolicy);
  
  repo->addComponent("move_serializer/simple", 
    new MoveSerializer("simple", validator));
  repo->addComponent("move_serializer/decorated", 
    new MoveSerializer("decorated", validator));
  MoveSerializer* san = new MoveSerializer("compact", validator);
  repo->addComponent("move_serializer/san", san);
  repo->addComponent("move_serializer/compact", san);
  King::self()->setCastlingRules(castling_rules);
  
  return repo;
}


