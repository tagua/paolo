/*
  Copyright (c) 2007 Paolo Capriotti <p.capriotti@gmail.com>
            (c) 2007 Maurizio Monge <maurizio.monge@kdemail.net>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
*/

#include "queen.h"
#include <core/move.h>
#include <core/board.h>
#include <core/state.h>

namespace Chess {

Queen::Queen() { }

QString Queen::name() const { return "queen"; }

bool Queen::canMove(const Piece& piece, const Piece& target,
                    Move& move, const IState* state) const {
  PathInfo path = state->board()->path(move.src(), move.dst());
  return path.valid() && path.clear() && target.color() != piece.color();
}

int Queen::index() const { return 90; }

Queen* Queen::self() {
  static Queen s_instance;
  return &s_instance;
}

}
