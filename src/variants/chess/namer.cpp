/*
  Copyright (c) 2007 Paolo Capriotti <p.capriotti@gmail.com>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
*/

#include "namer.h"
#include <core/color.h>
#include <core/piece.h>
#include <core/type.h>

namespace Chess {

QString Namer::name(const Piece& piece) const {
  if (piece.color() && piece.type())
    return piece.color()->name() + "_" + piece.type()->name();
  else
    return "";
}

} // namespace Chess


