/*
  Copyright (c) 2007 Paolo Capriotti <p.capriotti@gmail.com>
            (c) 2007 Maurizio Monge <maurizio.monge@kdemail.net>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
*/

#ifndef CHESS__TYPES__KNIGHT_H
#define CHESS__TYPES__KNIGHT_H

#include <core/component.h>
#include <core/type.h>

namespace Chess {

class TAGUA_EXPORT Knight : public Component, public IType {
Q_OBJECT
  Knight();
public:
  virtual QString name() const;
  virtual bool canMove(const Piece& piece, const Piece& target,
                       Move& move, const IState* state) const;
  virtual int index() const;
  static Knight* self();
};

}

#endif // CHESS__TYPES__KNIGHT_H
