/*
  Copyright (c) 2007 Paolo Capriotti <p.capriotti@gmail.com>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
*/

#include <core/type.h>
#include <core/color.h>

namespace Crazyhouse {

extern const IType* pawn;
extern const IType* king;
extern const IType* queen;
extern const IType* bishop;
extern const IType* rook;
extern const IType* knight;
extern const IColor* players[];

}

