/*
  Copyright (c) 2007 Paolo Capriotti <p.capriotti@gmail.com>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
*/

#include "validator.h"

#include <KDebug>

#include <core/board.h>
#include <core/move.h>
#include <core/poolcollection.h>
#include <core/pool.h>
#include <core/state.h>

#include "global.h"

namespace Crazyhouse {

Validator::Validator(IValidator* validator)
: Delegators::Validator(validator) { }

Validator::~Validator() { }

bool Validator::pseudolegal(const IState* state, Move& move) const {
  // add drop information to move, if missing
  if (move.drop() == Piece() && 
      move.pool() && move.index() != -1) {
    move.setDrop(state->pools()->pool(move.pool())->get(move.index()));
  }

  Piece dropped = move.drop();
  if (dropped == Piece()) {
    return m_validator->pseudolegal(state, move);
  }
  else {
    // dropping on a valid square
    if (!state->board()->valid(move.dst()))
      return false;

    // cannot drop on occupied squares
    if (state->board()->get(move.dst()) != Piece())
      return false;
      
    // cannot drop pawns in first or eighth rank
    if (dropped.type() == pawn &&
          (move.dst().y == state->rank(0, players[0]) || 
           move.dst().y == state->rank(state->board()->size().y - 1, players[0])))
      return false;
      
    return true;
  }
}

const IColor* Validator::mover(const IState* state, const Move& move) const {
  if (move.drop() != Piece())
    return move.drop().color();
  else
    return m_validator->mover(state, move);
}

} // namespace Crazyhouse


