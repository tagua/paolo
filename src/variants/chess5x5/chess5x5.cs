using Tagua.Component;
using Tagua.Repository;
using Tagua.Point;
using Tagua.IVariantLoader;

public class Chess5x5 {
  public static Repository initrepo(IVariantLoader loader) {
    Repository repo = new Repository();
    Repository chess = loader.getRepository("chess");
    if (chess == null) return null;
    
    repo.setProxy(chess);
    
    Component chess_state_factory = chess.getComponent("state_factory");
    Component state_factory = chess_state_factory.invoke(
      "createFactory", new Point(5, 5));
    
    repo.addComponent("state_factory", state_factory);
    
    return repo;
  }
}
