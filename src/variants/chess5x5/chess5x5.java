package org.tagua-project.variants.chess5x5;

import org.tagua-project.core.Component;
import org.tagua-project.core.Repository;
import org.tagua-project.core.Point;
import org.tagua-project.core.IVariantLoader;

public class Chess5x5 {
  public static Repository initrepo(IVariantLoader loader) {
    Repository repo = new Repository();
    Repository chess = loader.getRepository("chess");
    if (chess == null) return null;
    
    repo.setProxy(chess);
    
    Component chess_state_factory = chess.getComponent("state_factory");
    Component state_factory = chess_state_factory.invoke(
      "createFactory", new Point(5, 5));
    
    repo.addComponent("state_factory", state_factory);
    
    return repo;
  }
}
