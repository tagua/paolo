/*
  Copyright (c) 2007 Paolo Capriotti <p.capriotti@gmail.com>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
*/

#ifndef COMPONENTS_H
#define COMPONENTS_H

#include <memory>
#include <QMap>

#include "variant.h"

class IAnimator;
class IAnimatorFactory;
class IColor;
class IMoveSerializer;
class INamer;
class IPolicy;
class IState;
class IValidator;
class TaguaAPI;
class Variant;

/**
  * @brief A utility class to simplify the interaction with variant components.
  * 
  * A component object holds all official components of a variant, and exposes
  * utility methods to retrieve them, or create them using the appropriate
  * factories.
  * 
  * Most methods return a component owned by the variant, but those that start
  * with 'create' use a factory to instantiate a new component that will be 
  * owned by the caller.
  */
class Components {
  std::auto_ptr<Variant> m_variant;

  IAnimatorFactory* m_animator_factory;
  IState* m_state;
  
  QMap<QString, IMoveSerializer*> m_move_serializers;
  INamer* m_namer;
  IPolicy* m_policy;
  IValidator* m_validator;
  QMap<int, IColor*> m_players;
public:
  explicit Components(Variant*);
  
  void setVariant(Variant*);

  IAnimator* createAnimator(TaguaAPI* api) const;
  IState* createState() const;
  
  IMoveSerializer* moveSerializer(const QString& type);
  INamer* namer();
  IPolicy* policy();
  IValidator* validator();
  IColor* player(int index);
  QMap<int, IColor*> players();
  
  Variant* variant();
};

#endif // COMPONENTS_H

