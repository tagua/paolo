/*
  Copyright (c) 2007 Paolo Capriotti <p.capriotti@gmail.com>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
*/

#ifndef ANIMATIONMANAGER_H
#define ANIMATIONMANAGER_H

#include <QString>
#include <core/animation_fwd.h>

class MainAnimation;
class NamedSprite;
class QPoint;

class AnimationManager {
  MainAnimation* m_main;
public:
  bool enabled;
  
  int maxSequence;
  bool movement;
  bool explode;
  bool fading;
  bool transform;
  
  AnimationManager();
  ~AnimationManager();
  
  void reload();
  void enqueue(const AnimationPtr& a);
  void stop();
  
  AnimationPtr group(const QString& flags) const;
  AnimationPtr appear(const NamedSprite& sprite,  const QString& flags) const;
  AnimationPtr disappear(const NamedSprite& sprite, const QString& flags) const;
  AnimationPtr move(const NamedSprite& sprite, 
                    const QPoint& destination, 
                    bool far,
                    const QString& flags) const;
  AnimationPtr morph(const NamedSprite& sprite1, 
                     const NamedSprite& sprite2, 
                     const QString& flags) const;
};

#endif // ANIMATIONMANAGER_H
