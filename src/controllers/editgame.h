/*
  Copyright (c) 2006 Paolo Capriotti <p.capriotti@gmail.com>
            (c) 2006 Maurizio Monge <maurizio.monge@kdemail.net>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
*/

#ifndef EDITGAMECONTROLLER_H
#define EDITGAMECONTROLLER_H

#include <map>
#include <set>
#include <boost/shared_ptr.hpp>
#include <core/state_fwd.h>
#include "abstract.h"
#include "agentgroup.h"

class Entity;
class Engine;
class Agent;
class IColor;
class ICSConnection;
class UserEntity;
class GraphicalGame;
class GraphicalSystem;
class Components;


/**
  * @brief A controller to edit or play games.
  *
  * EditGameController is used to play games, either locally or
  * on an ICS server. A pair of entities @code m_players, representing
  * the two players, is mantained as well as a set of additional entities,
  * @code m_entities.
  * If an entity needs to be notified about how the game progresses, one
  * should register it as an agent, adding it to @code m_agents.
  */
class EditGameController : public Controller {
  void init(const StatePtr& state);
  boost::shared_ptr<Agent> m_clock_agent;
  boost::shared_ptr<Agent> m_update_agent;
  typedef std::map<const IColor*, boost::shared_ptr<Entity> > Players;
protected:
  virtual void onNavigation();

  boost::shared_ptr<UserEntity> m_entity;
  boost::shared_ptr<GraphicalSystem> m_graphical;
  AgentGroup m_agents;
  Components* m_components;
  boost::shared_ptr<GraphicalGame> m_game;
  virtual boost::shared_ptr<UserEntity> entity() const { return m_entity; }
  Players m_players;
  std::set<boost::shared_ptr<Entity> > m_entities;
  
  bool detachAllPlayerEntities();
public:
  EditGameController(ChessTable*, const StatePtr& startingPos = StatePtr());
  ~EditGameController();

  virtual ActionCollection* variantActions() const;
  virtual QString variant() const;
  EntityToken addPlayingEngine(const IColor* side, const boost::shared_ptr<Engine>& engine);
//   EntityToken addAnalysingEngine(const boost::shared_ptr<Engine>& engine);
  void removeEntity(const EntityToken& token);
  bool addICSPlayer(const IColor* side, int game_number, 
                    const boost::shared_ptr<ICSConnection>& connection);
  bool setExaminationMode(int game_number, const boost::shared_ptr<ICSConnection>& connection);
  bool setObserveMode(int game_number, const boost::shared_ptr<ICSConnection>& connection);

  virtual void loadPGN(const PGN&);

  virtual void createCtrlAction();
  virtual void destroyCtrlAction();

  virtual boost::shared_ptr<Controller> end();
  virtual void detach();
  
  virtual void reloadSettings();
  virtual void setUI(UI& ui);
  virtual void activate();
};


#endif
